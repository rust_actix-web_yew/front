
#!/usr/bin/env bash

cd /home/rust_yew/front

if lsof -Pi :8082 -sTCP:LISTEN -t >/dev/null ; then
    echo "Port 8082 is in use. Killing the process..."
    killall -9 $(lsof -t -i:8080)
else
    echo "Port 8082 is not in use."
fi
