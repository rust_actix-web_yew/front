# front with yew in rust
* $ cargo new front

## dependencies
* $ rustup target add wasm32-unknown-unknown
* $ cargo install trunk

* $ cargo install lightningcss --version 1.0.0-alpha.54


* cargo install --git https://github.com/trunk-rs/trunk trunk

## run front
* $ trunk serve

- http://0.0.0.0:8082/
- http://95.216.171.46:8082/
