use reqwasm::http::Request;
use serde::Deserialize;
use yew::prelude::*;

#[derive(Clone, PartialEq, Deserialize)]
struct User {
    pub name: String,
    pub email: String,    
}

#[derive(Properties, PartialEq)]
struct UserListProps {
    user: Vec<User>,
}

#[function_component(UsersList)]
fn user_list(UserListProps { user }: &UserListProps) -> Html {
    user.iter()
        .map(|user| {
            let user = user.clone();
            html! {
                <p>{format!("{} - {}",
                user.name,                
                user.email,                
            )}</p>
            }
        })
        .collect()
}


#[function_component(Get)]
pub fn app() -> Html {
    
    let user = use_state(|| vec![]);
    {
        let user = user.clone();
        use_effect_with_deps(
            move |_| {
                let user = user.clone();
                wasm_bindgen_futures::spawn_local(async move {
                    let fetched_data: Vec<User> = Request::get("http://0.0.0.0:8081/get_all")
                        .send()
                        .await
                        .unwrap()
                        .json()
                        .await
                        .unwrap();
                    user.set(fetched_data);
                });
                || ()
            },
            (),
        );
    }

    html! {
        <>               
            <div>                
                <UsersList user={(*user).clone()} />
            </div>
            
        </>
    }
}
