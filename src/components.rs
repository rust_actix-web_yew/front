use yew::prelude::*;

use wasm_bindgen::JsCast;
use wasm_bindgen::UnwrapThrowExt;
use web_sys::Event;
use web_sys::HtmlInputElement;
use web_sys::InputEvent;


fn get_value_from_input_event(e: InputEvent) -> String {
    let event: Event = e.dyn_into().unwrap_throw();
    let event_target = event.target().unwrap_throw();
    let target: HtmlInputElement = event_target.dyn_into().unwrap_throw();
    web_sys::console::log_1(&target.value().into());
    target.value()
}

// COMPONENT TextInputName
#[derive(Clone, PartialEq, Properties)]
pub struct PropsName {
    pub value_name: String,
    pub on_change_name: Callback<String>,
}

#[function_component(TextInputName)]
pub fn text_input_name(props: &PropsName) -> Html {
    let PropsName {
        value_name,
        on_change_name,
    } = props.clone();
    let oninput = Callback::from(move |input_event: InputEvent| {
        on_change_name.emit(get_value_from_input_event(input_event));
    });
    html! {
        <input type="text" {value_name} {oninput} />
    }
}

// COMPONENT TextInputEmail
#[derive(Clone, PartialEq, Properties)]
pub struct PropsEmail {
    pub value_email: String,
    pub on_change_email: Callback<String>,
}

#[function_component(TextInputEmail)]
pub fn text_input_email(props: &PropsEmail) -> Html {
    let PropsEmail {
        value_email,
        on_change_email,
    } = props.clone();
    let oninput = Callback::from(move |input_event: InputEvent| {
        on_change_email.emit(get_value_from_input_event(input_event));
    });
    html! {
        <input type="text" {value_email} {oninput} />
    }
}

