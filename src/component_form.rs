use yew::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen::UnwrapThrowExt;
use web_sys::Event;
use web_sys::HtmlInputElement;
use web_sys::InputEvent;


fn get_value_from_input_event(e: InputEvent) -> String {
    let event: Event = e.dyn_into().unwrap_throw();
    let event_target = event.target().unwrap_throw();
    let target: HtmlInputElement = event_target.dyn_into().unwrap_throw();    
    target.value()
}

#[derive(Clone, PartialEq, Properties)]
pub struct PropsName {
    pub name: String,    
    pub on_change_name: Callback<String>,
}

#[derive(Clone, PartialEq, Properties)]
pub struct PropsEmail {
    pub email: String,
    pub on_change_email: Callback<String>,
}

#[function_component(TextInputName)]
pub fn text_input_name(props: &PropsName) -> Html {
    let PropsName {
        name,  
        on_change_name,      
    } = props.clone();   
    let oninput = Callback::from(move |input_event: InputEvent| {
        on_change_name.emit(get_value_from_input_event(input_event));
    }); 
    html! {        
        <input type="text" {name} {oninput} />
    }
}

#[function_component(TextInputEmail)]
pub fn text_input_email(props: &PropsEmail) -> Html {
    let PropsEmail {
        email,   
        on_change_email,       
    } = props.clone();   
    let oninput = Callback::from(move |input_event: InputEvent| {
        on_change_email.emit(get_value_from_input_event(input_event));
    });  
    html! {        
        <input type="text" {email} {oninput}/>
    }
}

#[derive(Clone, PartialEq, Properties)]
pub struct FormProps {    
    pub name: String,
    pub email: String,
    pub on_change_name: Callback<String>,
    pub on_change_email: Callback<String>,    
}

#[function_component(Form)]
pub fn form(props: &FormProps) -> Html {
    let header = "Form Component";
    html! {
        <main>
            <br/><hr/><br/>
            <div>
                <h2>{ header }</h2>
            </div>
            <div>
                {"Enter a Name: "}                    
                <TextInputName 
                on_change_name={props.on_change_name.clone()}                
                name={props.name.clone()}                 
                />
                
            </div>
            <b></b>
            <div>
                {"Enter a Email: "}                    
                <TextInputEmail    
                on_change_email={props.on_change_email.clone()}                
                email={props.email.clone()} />
            </div>
        </main>
    }
}
