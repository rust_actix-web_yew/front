use yew_router::prelude::*;
use yew::html::Scope;

use yew::prelude::*;
use serde_wasm_bindgen::to_value;
use web_sys::{Request, RequestInit, RequestMode};
use serde_json::json;

use log::info;

mod components;
use components::{TextInputEmail, TextInputName};

mod component_form;
//use component_form::Form;

mod component_get;
use component_get::Get;

#[derive(Debug, Clone, Copy, PartialEq, Routable)]
enum Route {
    #[at("/")]
    Home,    
    //#[at("/form")]
    //Form,    
    #[at("/get")]
    Get,      
    //#[not_found]
    //#[at("/404")]
    //NotFound,
}


pub enum Msg {
    ToggleNavbar,
    Save,
    Cancel,
    SetName(String),
    SetEmail(String),    
}

#[derive(Clone, PartialEq)]
pub struct Main {
    navbar_active: bool,
    pub name: String,
    pub phone: String,
    pub email: String,
    pub comments: String,
}


impl Component for Main {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            navbar_active: false,
            name: String::from(""),
            phone: String::from(""),
            email: String::from(""),
            comments: String::from(""),            
        }
    }
    
    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {

            Msg::ToggleNavbar => {
                self.navbar_active = !self.navbar_active;
                true
            }

            Msg::SetName(name) => {
                self.name = name;
                false
            }

            Msg::SetEmail(email) => {
                self.email = email;
                false
            }                                            

            Msg::Save => {
                let data = json!({
                    "name": self.name,
                    "phone": "989898989",
                    "email": self.email,
                    "comments": "nada", //self.comments,
                });

                // Create a request
                let mut opts = RequestInit::new();                                  
                let data_str = serde_json::to_string(&data).unwrap();
                
                opts.method("POST");
                opts.mode(RequestMode::Cors);                
                opts.headers(&to_value(&json!({
                    "Content-Type": "application/json",
                })).unwrap());
                opts.body(Some(&to_value(&data_str).unwrap()));                                
                
                let window = web_sys::window().unwrap();                                          
                let request = Request::new_with_str_and_init("http://127.0.0.1:8081/post_one", &opts)
                    .expect("Failed to create request");  //get the error
                    
                // Send the request
                let _ = window.fetch_with_request(&request);                                                                

                info!("Data save on postgres {}", data_str.as_str());                
                true
            }

            Msg::Cancel => {
                //some action
                println!("name => {}", self.name);
                false
            }
        }
    }


    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();  // capturar los eventos
        let header = "Form - Rust (Actix-web & Yew) - Main Component";

        // form 1
        // let on_change_name_1 = ctx.link().callback(Msg::SetName);
        // let on_change_email_1 = ctx.link().callback(Msg::SetEmail);
        
        // form 2
        let on_change_name = ctx.link().callback(Msg::SetName);
        let on_change_email = ctx.link().callback(Msg::SetEmail);
                
        html! {                                                 
                <BrowserRouter>  
                    { self.view_nav(ctx.link()) }

                    <main>
                        <Switch<Route> render={Switch::render(switch)} /> 
                    </main>
                
                                                            
                    <main>
                        // <div>
                        //    <h3 class="red">{ header }</h3>
                        // </div>

                        <div>                
                            /*
                            // form: external component, to be shown on menu                    
                            <Form                                                
                                on_change_name={on_change_name_1}
                                on_change_email={on_change_email_1}
                                name={self.name.clone()}                                      
                                email={self.email.clone()}
                            /> 
                            */                                                                             
                             
                            // Home direct, we can add a new record from Home as well with others components
                            <main>
                                <br/><hr/><br/>
                                <div>
                                    <h4>{ header }</h4>
                                </div>
                                <div>
                                    {"Enter a new Name: "}
                                    <TextInputName {on_change_name} value_name={self.name.clone()} />                                                                            
                                </div>
                                <b></b>
                                <div>
                                    {"Enter a new Email: "}
                                    <TextInputEmail {on_change_email} value_email={self.email.clone()} />                                    
                                </div>
                            </main>   

                        </div>

                        <div>
                            <button onclick ={link.callback(|_| Msg::Save)}>{ "Save" }</button>
                            <button onclick ={link.callback(|_| Msg::Cancel)}>{ "Cancel" }</button>            
                        </div>

                        /*
                        <div>                       
                            <Get />                        
                        </div>
                         */

                    </main>     
                    
                </BrowserRouter>                   
                        
        }
    }
}


impl Main {
    fn view_nav(&self, _link: &Scope<Self>) -> Html {
        let Self { navbar_active, .. } = *self;

        let active_class = if !navbar_active { "is-active" } else { "" };

        html! {
            <nav class="navbar is-primary" role="navigation" aria-label="main navigation">

                <div class={classes!("navbar-menu", active_class)}>
                    <div class="navbar-start" style="display: flex; gap: 1rem;">
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Home}>
                            { "Home" }
                        </Link<Route>>

                        /*
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Form}>
                            { "Form" }
                        </Link<Route>>
                        */                        
                                           
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Get}>
                            { "List of records - Get Component" }
                        </Link<Route>>
                        
                    </div>
                </div>
            </nav>
        }
    }
}


fn switch(routes: &Route) -> Html {
    match routes.clone() {
        Route::Home => html! { <h3>{ "" }</h3> },        
        //Route::Form => html! {
        //    <Get />
        //},
        Route::Get => html! { 
            <>
                <h3>{ "List" }</h3>
                <Get />
            </>
         },           
        // Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}


fn main() {
    // initilize logging
    wasm_logger::init(wasm_logger::Config::default());    

    yew::start_app::<Main>();    
}
